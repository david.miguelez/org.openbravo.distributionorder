/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2017 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */

package org.openbravo.distributionorder.test.utils;

import java.math.BigDecimal;

import org.openbravo.dal.service.OBDal;
import org.openbravo.distributionorder.DistributionOrderLine;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.plm.AttributeSetInstance;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.common.uom.UOM;

public class DistributionOrderLineData {
  private Organization organization;
  private Product product;
  private UOM uom;
  private long lineNo;
  private DistributionOrderLine referencedDistOrderLine;
  private BigDecimal qtyOrdered;
  private BigDecimal qtyConfirmed;
  private UOM aum;
  private BigDecimal operativeQty;
  private AttributeSetInstance attributeSetInstance;

  private DistributionOrderLineData(
      DistributionOrderLineDataBuilder distributionOrderLineDataBuilder) {
    this.organization = distributionOrderLineDataBuilder.organization;
    this.product = distributionOrderLineDataBuilder.product;
    this.uom = distributionOrderLineDataBuilder.uom;
    this.lineNo = distributionOrderLineDataBuilder.lineNo;
    this.referencedDistOrderLine = distributionOrderLineDataBuilder.referencedDistOrderLine;
    this.qtyOrdered = distributionOrderLineDataBuilder.qtyOrdered;
    this.qtyConfirmed = distributionOrderLineDataBuilder.qtyConfirmed;
    this.aum = distributionOrderLineDataBuilder.aum;
    this.operativeQty = distributionOrderLineDataBuilder.operativeQty;
    this.attributeSetInstance = distributionOrderLineDataBuilder.attributeSetInstance;
  }

  public Organization getOrganization() {
    return organization;
  }

  public Product getProduct() {
    return product;
  }

  public UOM getUom() {
    return uom;
  }

  public long getLineNo() {
    return lineNo;
  }

  public DistributionOrderLine getReferencedDistOrderLine() {
    return referencedDistOrderLine;
  }

  public BigDecimal getQtyOrdered() {
    return qtyOrdered;
  }

  public BigDecimal getQtyConfirmed() {
    return qtyConfirmed;
  }

  public UOM getAum() {
    return aum;
  }

  public BigDecimal getOperativeQty() {
    return operativeQty;
  }

  public AttributeSetInstance getAttributeSetInstance() {
    return attributeSetInstance;
  }

  public static class DistributionOrderLineDataBuilder {
    private Organization organization;
    private Product product;
    private UOM uom;
    private long lineNo;
    private DistributionOrderLine referencedDistOrderLine;
    private BigDecimal qtyOrdered;
    private BigDecimal qtyConfirmed;
    private UOM aum;
    private BigDecimal operativeQty;
    private AttributeSetInstance attributeSetInstance;

    public DistributionOrderLineDataBuilder() {

    }

    public DistributionOrderLineDataBuilder organizationID(String newOrganizationID) {
      Organization newOrganization = OBDal.getInstance().get(Organization.class, newOrganizationID);
      this.organization = newOrganization;
      return this;
    }

    public DistributionOrderLineDataBuilder product(String newProductID) {
      Product newProduct = OBDal.getInstance().get(Product.class, newProductID);
      this.product = newProduct;
      return this;
    }

    public DistributionOrderLineDataBuilder uom(String newUOMID) {
      UOM newUOM = OBDal.getInstance().get(UOM.class, newUOMID);
      this.uom = newUOM;
      return this;
    }

    public DistributionOrderLineDataBuilder lineNo(long newLineNo) {
      this.lineNo = newLineNo;
      return this;
    }

    public DistributionOrderLineDataBuilder referencedDistOrderLine(String newDistOrderLineID) {
      DistributionOrderLine newDistOrderLine = OBDal.getInstance()
          .get(DistributionOrderLine.class, newDistOrderLineID);
      this.referencedDistOrderLine = newDistOrderLine;
      return this;
    }

    public DistributionOrderLineDataBuilder qtyOrdered(BigDecimal newQtyOrdered) {
      this.qtyOrdered = newQtyOrdered;
      return this;
    }

    public DistributionOrderLineDataBuilder qtyConfirmed(BigDecimal newQtyConfirmed) {
      this.qtyConfirmed = newQtyConfirmed;
      return this;
    }

    public DistributionOrderLineDataBuilder aumId(String newAumId) {
      UOM newAum = OBDal.getInstance().get(UOM.class, newAumId);
      this.aum = newAum;
      return this;
    }

    public DistributionOrderLineDataBuilder operativeQty(BigDecimal newoperativeQty) {
      this.operativeQty = newoperativeQty;
      return this;
    }

    public DistributionOrderLineDataBuilder attributeSetInstance(
        AttributeSetInstance newAttributeSetInstance) {
      this.attributeSetInstance = newAttributeSetInstance;
      return this;
    }

    public DistributionOrderLineData build() {
      return new DistributionOrderLineData(this);
    }

  }

}
