/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2017 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */

package org.openbravo.distributionorder.test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.codehaus.jettison.json.JSONException;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openbravo.base.exception.OBException;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.distributionorder.DistributionOrder;
import org.openbravo.distributionorder.DistributionOrderLine;
import org.openbravo.distributionorder.test.utils.DistributionOrderHeaderData;
import org.openbravo.distributionorder.test.utils.DistributionOrderLineData;
import org.openbravo.distributionorder.test.utils.DistributionOrderTestUtils;
import org.openbravo.distributionorder.test.utils.IssueOrReceiptDOHeaderData;
import org.openbravo.distributionorder.test.utils.IssueOrReceiptDOLineData;
import org.openbravo.distributionorder.test.utils.ShipmentInOutHeaderData;
import org.openbravo.distributionorder.test.utils.ShipmentInOutLineData;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.materialmgmt.transaction.InternalMovement;
import org.openbravo.model.materialmgmt.transaction.InternalMovementLine;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class OBDOComplexTests extends OBDO_FB_WeldBaseTest {

  @Before
  public void initialize() {
    log.info("Initializing Distribution Orders Complex Tests...");
    super.initialize();
  }

  /**
   * This test case checks that the basic flow works correctly for Documents with several lines
   * <ul>
   * <li>1. Create a Distribution Order Receipt with 10 lines</li>
   * <li>2. Check that the related Distribution Order Issue has been correctly created</li>
   * <li>3. Create an Issue Distribution Order related to the Distribution Order Issue</li>
   * <li>3. Create an Receive Distribution Order related to the Issue Distribution Order</li>
   * </ul>
   */

  @Test
  public void DOC010_CompleteDistributionOrdersSeveraLines() {
    OBContext.setAdminMode();
    try {
      List<Product> productList = getProductListForDOC010();

      DistributionOrder distributionOrderReceipt = createDOReceiptDOC010(productList);
      verifyDOIssueCorrectlyCreatedForDOC010(distributionOrderReceipt);
      DistributionOrder distributionOrderIssue = distributionOrderReceipt.getReferencedDistorder();

      updateConfirmedQtyForDOC010(distributionOrderIssue);

      InternalMovement issueDistributionOrder = createIssueDOForDOC010(distributionOrderIssue,
          100L);

      createReceiveDOForDOC010(issueDistributionOrder, 100L);

    } catch (Exception e) {
      log.error(e.getMessage(), e);
      throw new OBException(e);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  private List<Product> getProductListForDOC010() {
    List<Product> productList = new ArrayList<Product>();
    for (int i = 0; i < 10; i++) {
      Product product = DistributionOrderTestUtils.cloneProduct(ALE_BEER_PRODUCT_ID, "DOC020/" + i);
      createStockForForTestDOC010(product);
      productList.add(product);
    }
    return productList;
  }

  private void createStockForForTestDOC010(Product product) {
    ShipmentInOut goodsReceipt = getShipmentInoutHeaderForDOC010();
    createAndInsertGoodsReceiptLinesForDOC010(goodsReceipt, product, new BigDecimal(10000));
    DistributionOrderTestUtils.processShipmentInOut(goodsReceipt);
  }

  private ShipmentInOut getShipmentInoutHeaderForDOC010() {
    String docNo = DistributionOrderTestUtils.getNextDocNoForShipmentInOut("DOC010");
    ShipmentInOutHeaderData shipmentInOutHeaderData = new ShipmentInOutHeaderData.ShipmentInOutHeaderDataBuilder()//
        .organizationID(DistributionOrderTestUtils.US_EAST_ORG_ID)//
        .documentNo(docNo)//
        .documentTypeID(DistributionOrderTestUtils.MM_RECEIPT_US_DOCTYPE_ID)//
        .warehouseID(DistributionOrderTestUtils.US_EC_WAREHOUSE_ID)//
        .businessPartnerID(BE_SOFT_BPARTNER_ID)//
        .build();
    return DistributionOrderTestUtils.createAndSaveGoodsReceiptHeader(shipmentInOutHeaderData);
  }

  private void createAndInsertGoodsReceiptLinesForDOC010(ShipmentInOut goodsReceipt,
      Product product, BigDecimal movementQty) {
    long lineNo = DistributionOrderTestUtils.getNextLineNoForGoodsReceipt(goodsReceipt);
    ShipmentInOutLineData shipmentInoutLineData = new ShipmentInOutLineData.ShipmentInOutLineDataBuilder()//
        .organizationID(DistributionOrderTestUtils.US_EAST_ORG_ID)//
        .lineNo(lineNo)//
        .productID(product.getId())//
        .movementQty(movementQty)//
        .uomID(DistributionOrderTestUtils.UNIT_UOM_ID)//
        .storageBinID(DistributionOrderTestUtils.EC_000_LOCATOR_ID)//
        .build();
    DistributionOrderTestUtils.createAndInsertGoodsReceiptLine(shipmentInoutLineData, goodsReceipt);
  }

  private DistributionOrder createDOReceiptDOC010(List<Product> productList) {
    DistributionOrder distributionOrder = getDistributionOrderReceiptHeaderDOC010();
    createAndInsertDistributionOrderLinesDOC010(distributionOrder, productList);
    DistributionOrderTestUtils.processDistributionOrderAndVerifyIsProcessed(distributionOrder);
    return distributionOrder;
  }

  private DistributionOrder getDistributionOrderReceiptHeaderDOC010() {
    String docNo = DistributionOrderTestUtils.getNextDocNoForDistributionOrder("DOC010");
    DocumentType doReceiptDocType = DistributionOrderTestUtils.getDistributionOrderReceiptDocType();
    DistributionOrderHeaderData distributionOrderHeaderData = new DistributionOrderHeaderData.DistributionOrderHeaderDataBuilder()//
        .organizationID(DistributionOrderTestUtils.US_WEST_ORG_ID)//
        .orderDate(new Date())//
        .scheduledDeliveryDate(new Date())//
        .warehouseReceiptID(DistributionOrderTestUtils.US_WC_WAREHOUSE_ID)//
        .warehouseIssueID(DistributionOrderTestUtils.US_EC_WAREHOUSE_ID)//
        .isSalesTransaction(false)//
        .docTypeID(doReceiptDocType.getId())//
        .docNo(docNo)//
        .description("DOC010 Test")//
        .build();
    return DistributionOrderTestUtils
        .createAndSaveDistributionOrderHeader(distributionOrderHeaderData);
  }

  private void createAndInsertDistributionOrderLinesDOC010(DistributionOrder distributionOrder,
      List<Product> productList) {
    long lineNo = DistributionOrderTestUtils.getNextLineNoForDistributionOrder(distributionOrder);
    for (Product product : productList) {
      DistributionOrderLineData distributionOrderLineData = new DistributionOrderLineData.DistributionOrderLineDataBuilder()//
          .organizationID(DistributionOrderTestUtils.US_WEST_ORG_ID)//
          .product(product.getId())//
          .uom(DistributionOrderTestUtils.UNIT_UOM_ID)//
          .lineNo(lineNo)//
          .qtyOrdered(new BigDecimal(10).add(new BigDecimal(lineNo)))//
          .build();
      DistributionOrderTestUtils.createAndInsertDistributionOrderLine(distributionOrderLineData,
          distributionOrder);
      lineNo += 10;
    }
  }

  private void verifyDOIssueCorrectlyCreatedForDOC010(DistributionOrder distributionOrder) {
    DistributionOrder distributionOrderIssue = distributionOrder.getReferencedDistorder();
    // Need to refresh the object in order to retrieve the latest changes done to it
    OBDal.getInstance().refresh(distributionOrderIssue);
    DistributionOrderTestUtils.assertDistributionOrderIsNotNull(distributionOrderIssue);
    DistributionOrderTestUtils.assertDistributionOrderCreatedIsRelatedToOriginalDO(
        distributionOrderIssue, distributionOrder);
    DistributionOrderTestUtils.assertDistributionOrderHasRequestedStatus(distributionOrderIssue);
    DistributionOrderTestUtils.assertThatDistributionOrderHasIssuedStatus(distributionOrderIssue,
        0L);
    DistributionOrderTestUtils.assertDistributionOrderHasNLines(distributionOrderIssue, 10);
  }

  private void updateConfirmedQtyForDOC010(DistributionOrder issueDistributionOrder) {
    List<DistributionOrderLine> issueDOLine = issueDistributionOrder
        .getOBDODistributionOrderLineList();
    for (DistributionOrderLine issueLine : issueDOLine) {
      DistributionOrderTestUtils.updateConfirmedQty(issueLine, issueLine.getOrderedQuantity());
    }
    DistributionOrderTestUtils
        .confirmDistributionOrderAndVerifyStatusUpdated(issueDistributionOrder);

  }

  private InternalMovement createIssueDOForDOC010(DistributionOrder distributionOrderIssue,
      long issuedPercentage) throws JSONException {
    InternalMovement issueDistributionOrder = getIssueDistributionOrderHeaderForDOC010(
        distributionOrderIssue);
    DistributionOrderTestUtils.processIssueDOAndVerifyIsProcessed(issueDistributionOrder);
    for (InternalMovementLine issueDOLine : issueDistributionOrder
        .getMaterialMgmtInternalMovementLineList()) {
      DistributionOrderTestUtils.assertThatIssueDoLineHasUpdatedRelatedDocs(issueDOLine,
          issueDOLine.getMovementQuantity(), issuedPercentage);
    }
    return issueDistributionOrder;
  }

  private InternalMovement getIssueDistributionOrderHeaderForDOC010(
      DistributionOrder distributionOrderIssue) {
    String docNo = DistributionOrderTestUtils.getNextDocNoForIssueOrReceiptDO("DOC010");
    IssueOrReceiptDOHeaderData issueOrReceiptDOHeaderData = new IssueOrReceiptDOHeaderData.IssueOrReceiptDOHeaderDataBuilder()//
        .organizationID(DistributionOrderTestUtils.US_EAST_ORG_ID)//
        .name(docNo)//
        .description("DOC010")//
        .movementDate(new Date())//
        .distributionOrderID(distributionOrderIssue.getId())//
        .inTransitBinID(DistributionOrderTestUtils.EC_200_LOCATOR_ID)//
        .isSalesTransaction(true)//
        .build();
    return DistributionOrderTestUtils
        .createAndSaveIssueOrReceiveDOHeader(issueOrReceiptDOHeaderData);
  }

  private InternalMovement createReceiveDOForDOC010(InternalMovement issueDistributionOrder,
      long receiveStatusPercentage) throws JSONException {
    InternalMovement receiveDistributionOrder = getReceiveDistributionOrderHeaderForDOC010(
        issueDistributionOrder);
    DistributionOrderTestUtils.processReceiveDOAndVerifyIsProcessed(receiveDistributionOrder,
        DistributionOrderTestUtils.WC_000_LOCATOR_ID);
    for (InternalMovementLine receiveDOLine : receiveDistributionOrder
        .getMaterialMgmtInternalMovementLineList()) {
      DistributionOrderTestUtils.assertThatReceiveDoLineHasUpdatedRelatedDocs(receiveDOLine,
          receiveDOLine.getMovementQuantity(), receiveStatusPercentage);
    }
    return issueDistributionOrder;
  }

  private InternalMovement getReceiveDistributionOrderHeaderForDOC010(
      InternalMovement issueDistributionOrder) {
    String issueDOViewID = DistributionOrderTestUtils
        .getIDOfViewRecordForIssueDO(issueDistributionOrder);
    String docNo = DistributionOrderTestUtils.getNextDocNoForIssueOrReceiptDO("DOC010");
    IssueOrReceiptDOHeaderData issueOrReceiptDOHeaderData = new IssueOrReceiptDOHeaderData.IssueOrReceiptDOHeaderDataBuilder()//
        .organizationID(DistributionOrderTestUtils.US_WEST_ORG_ID)//
        .name(docNo)//
        .description("DOC010")//
        .movementDate(new Date())//
        .isSalesTransaction(false)//
        .issueDOViewID(issueDOViewID)//
        .build();
    return DistributionOrderTestUtils
        .createAndSaveIssueOrReceiveDOHeader(issueOrReceiptDOHeaderData);
  }

  /**
   * This test checks that it is possible to correctly complete de Distribution Order flow with
   * partial quantities in the documents involved
   * <ul>
   * <li>1. Clone a Product to create a new one and create Stock for it</li>
   * <li>2. Create and complete a Distribution Order Receipt of 10 units for that Product</li>
   * <li>3. Check that the related Distribution Order Issue has been created correctly</li>
   * <li>4. Confirm 8 units in the Distribution Order Issue and check that the Distribution Order
   * Receipt has been updated properly</li>
   * <li>5. Create an Issue Distribution Order of 5 Units and complete it. Check the DO Documents
   * are updated</li>
   * <li>6. Create an Issue Distribution Order of 1 Units and complete it. Check the DO Documents
   * are updated</li>
   * <li>7. Create an Issue Distribution Order of 1 Units and complete it. Check the DO Documents
   * are updated</li>
   * <li>8. Close the Distribution Order Issue and check that there is no remaining quantity to be
   * sent</li>
   * <li>9. Create a Receive Distribution Order for 5 units and check that the related DO Documents
   * are upated</li>
   * <li>10. Create a Receive Distribution Order for 1 units and check that the related DO Documents
   * are upated</li>
   * <li>11. Create a Receive Distribution Order for 1 units and check that the related DO Documents
   * are upated</li>
   * </ul>
   */
  @Test
  public void DOC020_CompleteDOFlowWithPartialQuantities() {
    OBContext.setAdminMode();
    try {
      Product product = DistributionOrderTestUtils.cloneProduct(ALE_BEER_PRODUCT_ID, "DOC020");
      createStockForForTestDOC020(product);

      DistributionOrder distributionOrderReceipt = createDOReceiptForDOC020(product);
      verifyDOIssueCorrectlyCreatedForDOC020(distributionOrderReceipt);

      DistributionOrder distributionOrderIssue = distributionOrderReceipt.getReferencedDistorder();
      updateConfirmedQtyForDOC020(distributionOrderIssue);

      InternalMovement issueDistributionOrder1 = createIssueDOForDOC020(distributionOrderIssue,
          product, new BigDecimal(5), new BigDecimal(5), 63L);
      InternalMovement issueDistributionOrder2 = createIssueDOForDOC020(distributionOrderIssue,
          product, BigDecimal.ONE, new BigDecimal(6), 75L);
      InternalMovement issueDistributionOrder3 = createIssueDOForDOC020(distributionOrderIssue,
          product, BigDecimal.ONE, new BigDecimal(7), 88L);

      DistributionOrderTestUtils
          .closeDistributionOrderAndCheckStatusUpdated(distributionOrderIssue);
      DistributionOrderTestUtils.assertThatDistributionOrderHasIssuedStatus(distributionOrderIssue,
          100L);

      createReceiveDOForDOC020(issueDistributionOrder1, new BigDecimal(5), 71L);
      createReceiveDOForDOC020(issueDistributionOrder2, new BigDecimal(6), 86L);
      createReceiveDOForDOC020(issueDistributionOrder3, new BigDecimal(7), 100L);

    } catch (Exception e) {
      log.error(e.getMessage(), e);
      throw new OBException(e);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  private void createStockForForTestDOC020(Product product) {
    ShipmentInOut goodsReceipt = getShipmentInoutHeaderForDOC020();
    createAndInsertGoodsReceiptLinesForDOC020(goodsReceipt, product, new BigDecimal(100));
    DistributionOrderTestUtils.processShipmentInOut(goodsReceipt);
  }

  private ShipmentInOut getShipmentInoutHeaderForDOC020() {
    String docNo = DistributionOrderTestUtils.getNextDocNoForShipmentInOut("DOC020");
    ShipmentInOutHeaderData shipmentInOutHeaderData = new ShipmentInOutHeaderData.ShipmentInOutHeaderDataBuilder()//
        .organizationID(DistributionOrderTestUtils.US_EAST_ORG_ID)//
        .documentNo(docNo)//
        .documentTypeID(DistributionOrderTestUtils.MM_RECEIPT_US_DOCTYPE_ID)//
        .warehouseID(DistributionOrderTestUtils.US_EC_WAREHOUSE_ID)//
        .businessPartnerID(BE_SOFT_BPARTNER_ID)//
        .build();
    return DistributionOrderTestUtils.createAndSaveGoodsReceiptHeader(shipmentInOutHeaderData);
  }

  private void createAndInsertGoodsReceiptLinesForDOC020(ShipmentInOut goodsReceipt,
      Product product, BigDecimal movementQty) {
    long lineNo = DistributionOrderTestUtils.getNextLineNoForGoodsReceipt(goodsReceipt);
    ShipmentInOutLineData shipmentInoutLineData = new ShipmentInOutLineData.ShipmentInOutLineDataBuilder()//
        .organizationID(DistributionOrderTestUtils.US_EAST_ORG_ID)//
        .lineNo(lineNo)//
        .productID(product.getId())//
        .movementQty(movementQty)
        .uomID(DistributionOrderTestUtils.UNIT_UOM_ID)//
        .storageBinID(DistributionOrderTestUtils.EC_000_LOCATOR_ID)//
        .build();
    DistributionOrderTestUtils.createAndInsertGoodsReceiptLine(shipmentInoutLineData, goodsReceipt);
  }

  private DistributionOrder createDOReceiptForDOC020(Product product) {
    DistributionOrder distributionOrder = getDistributionOrderReceiptHeaderForDOC020();
    createAndInsertDistributionOrderLinesForDOC020(distributionOrder, product);
    DistributionOrderTestUtils.processDistributionOrderAndVerifyIsProcessed(distributionOrder);
    return distributionOrder;
  }

  private DistributionOrder getDistributionOrderReceiptHeaderForDOC020() {
    String docNo = DistributionOrderTestUtils.getNextDocNoForDistributionOrder("DOC020");
    DocumentType doReceiptDocType = DistributionOrderTestUtils.getDistributionOrderReceiptDocType();
    DistributionOrderHeaderData distributionOrderHeaderData = new DistributionOrderHeaderData.DistributionOrderHeaderDataBuilder()//
        .organizationID(DistributionOrderTestUtils.US_ORG_ID)//
        .orderDate(new Date())//
        .scheduledDeliveryDate(new Date())//
        .warehouseReceiptID(DistributionOrderTestUtils.US_WC_WAREHOUSE_ID)//
        .warehouseIssueID(DistributionOrderTestUtils.US_EC_WAREHOUSE_ID)//
        .isSalesTransaction(false)//
        .docTypeID(doReceiptDocType.getId())//
        .docNo(docNo)//
        .description("DOC020 Test")//
        .build();
    return DistributionOrderTestUtils
        .createAndSaveDistributionOrderHeader(distributionOrderHeaderData);
  }

  private void createAndInsertDistributionOrderLinesForDOC020(DistributionOrder distributionOrder,
      Product product) {
    long lineNo = DistributionOrderTestUtils.getNextLineNoForDistributionOrder(distributionOrder);
    DistributionOrderLineData distributionOrderLineData = new DistributionOrderLineData.DistributionOrderLineDataBuilder()//
        .organizationID(DistributionOrderTestUtils.US_ORG_ID)//
        .product(product.getId())//
        .uom(DistributionOrderTestUtils.UNIT_UOM_ID)//
        .lineNo(lineNo)//
        .qtyOrdered(new BigDecimal(10))//
        .build();
    DistributionOrderTestUtils.createAndInsertDistributionOrderLine(distributionOrderLineData,
        distributionOrder);
  }

  private void verifyDOIssueCorrectlyCreatedForDOC020(DistributionOrder distributionOrder) {
    DistributionOrder distributionOrderIssue = distributionOrder.getReferencedDistorder();
    // Need to refresh the object in order to retrieve the latest changes done to it
    OBDal.getInstance().refresh(distributionOrderIssue);
    DistributionOrderTestUtils.assertDistributionOrderIsNotNull(distributionOrderIssue);
    DistributionOrderTestUtils.assertDistributionOrderCreatedIsRelatedToOriginalDO(
        distributionOrderIssue, distributionOrder);
    DistributionOrderTestUtils.assertDistributionOrderHasRequestedStatus(distributionOrderIssue);
    DistributionOrderTestUtils.assertThatDistributionOrderHasIssuedStatus(distributionOrderIssue,
        0L);
    DistributionOrderTestUtils.assertDistributionOrderHasNLines(distributionOrderIssue, 1);
  }

  private void updateConfirmedQtyForDOC020(DistributionOrder issueDistributionOrder) {
    DistributionOrderLine issueDOLine = issueDistributionOrder.getOBDODistributionOrderLineList()
        .get(0);
    DistributionOrderTestUtils.updateConfirmedQtyAndVerifyRelatedDOIsUpdated(issueDOLine,
        new BigDecimal(8));
  }

  private InternalMovement createIssueDOForDOC020(DistributionOrder distributionOrderIssue,
      Product product, BigDecimal issuedQty, BigDecimal totalIssuedQty, long issuedPercentage)
      throws JSONException {
    InternalMovement issueDistributionOrder = getIssueDistributionOrderHeaderForDOC020(
        distributionOrderIssue);
    createAndInsertIssueDOLinesForDOC020(issueDistributionOrder,
        distributionOrderIssue.getOBDODistributionOrderLineList().get(0), product, issuedQty);
    DistributionOrderTestUtils.processIssueDOAndVerifyIsProcessed(issueDistributionOrder);
    DistributionOrderTestUtils.assertThatIssueDoLineHasUpdatedRelatedDocs(
        issueDistributionOrder.getMaterialMgmtInternalMovementLineList().get(0), totalIssuedQty,
        issuedPercentage);
    return issueDistributionOrder;
  }

  private InternalMovement getIssueDistributionOrderHeaderForDOC020(
      DistributionOrder distributionOrderIssue) {
    String docNo = DistributionOrderTestUtils.getNextDocNoForIssueOrReceiptDO("DOC020");
    IssueOrReceiptDOHeaderData issueOrReceiptDOHeaderData = new IssueOrReceiptDOHeaderData.IssueOrReceiptDOHeaderDataBuilder()//
        .organizationID(DistributionOrderTestUtils.US_EAST_ORG_ID)//
        .name(docNo)//
        .description("DOC020")//
        .movementDate(new Date())//
        .distributionOrderID(distributionOrderIssue.getId())//
        .inTransitBinID(DistributionOrderTestUtils.EC_200_LOCATOR_ID)//
        .isSalesTransaction(true)//
        .build();
    return DistributionOrderTestUtils
        .createAndSaveIssueOrReceiveDOHeader(issueOrReceiptDOHeaderData);
  }

  private void createAndInsertIssueDOLinesForDOC020(InternalMovement issueDistributionOrder,
      DistributionOrderLine distributionOrderLine, Product product, BigDecimal issuedQty) {
    long lineNo = DistributionOrderTestUtils
        .getNextLineNoForIssueOrReceiptDO(issueDistributionOrder);
    IssueOrReceiptDOLineData issueOrReceiptDOLineData = new IssueOrReceiptDOLineData.IssueOrReceiptDOLineDataBuilder()//
        .organizationID(DistributionOrderTestUtils.US_EAST_ORG_ID)//
        .lineNo(lineNo)//
        .productID(product.getId())//
        .uomID(DistributionOrderTestUtils.UNIT_UOM_ID)//
        .movementQty(issuedQty)//
        .storageBinFromID(DistributionOrderTestUtils.EC_000_LOCATOR_ID)//
        .storageBinToID(DistributionOrderTestUtils.EC_200_LOCATOR_ID)//
        .distributionOrderLineID(distributionOrderLine.getId())//
        .build();
    DistributionOrderTestUtils.createAndInsertIssueOrReceiptDOLine(issueOrReceiptDOLineData,
        issueDistributionOrder);
  }

  private InternalMovement createReceiveDOForDOC020(InternalMovement issueDistributionOrder,
      BigDecimal totalReceivedQty, long receiveStatusPercentage) throws JSONException {
    InternalMovement receiveDistributionOrder = getReceiveDistributionOrderHeaderForDOC020(
        issueDistributionOrder);
    DistributionOrderTestUtils.processReceiveDOAndVerifyIsProcessed(receiveDistributionOrder,
        DistributionOrderTestUtils.WC_000_LOCATOR_ID);
    DistributionOrderTestUtils.assertThatReceiveDoLineHasUpdatedRelatedDocs(
        receiveDistributionOrder.getMaterialMgmtInternalMovementLineList().get(0), totalReceivedQty,
        receiveStatusPercentage);
    return issueDistributionOrder;
  }

  private InternalMovement getReceiveDistributionOrderHeaderForDOC020(
      InternalMovement issueDistributionOrder) {
    String issueDOViewID = DistributionOrderTestUtils
        .getIDOfViewRecordForIssueDO(issueDistributionOrder);
    String docNo = DistributionOrderTestUtils.getNextDocNoForIssueOrReceiptDO("DOC020");
    IssueOrReceiptDOHeaderData issueOrReceiptDOHeaderData = new IssueOrReceiptDOHeaderData.IssueOrReceiptDOHeaderDataBuilder()//
        .organizationID(DistributionOrderTestUtils.US_WEST_ORG_ID)//
        .name(docNo)//
        .description("DOC020")//
        .movementDate(new Date())//
        .isSalesTransaction(false)//
        .issueDOViewID(issueDOViewID)//
        .build();
    return DistributionOrderTestUtils
        .createAndSaveIssueOrReceiveDOHeader(issueOrReceiptDOHeaderData);
  }

}
