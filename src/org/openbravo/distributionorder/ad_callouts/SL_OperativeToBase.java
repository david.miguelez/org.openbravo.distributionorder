/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2017 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */

package org.openbravo.distributionorder.ad_callouts;

import java.math.BigDecimal;

import javax.servlet.ServletException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.openbravo.base.exception.OBException;
import org.openbravo.dal.core.OBContext;
import org.openbravo.erpCommon.ad_callouts.SimpleCallout;
import org.openbravo.materialmgmt.UOMUtil;

public class SL_OperativeToBase extends SimpleCallout {
  private static final String DOr_ID = "DB37310EEDA54A0FBBA437DBF940D303";
  private static final String DOi_ID = "A28B6814DD5C4B3E9481E899086B99D1";
  private static final Logger logger = Logger.getLogger(SL_OperativeToBase.class);

  @Override
  protected void execute(CalloutInfo info) throws ServletException {

    String strWindowId = info.getWindowId();
    BigDecimal qty = info.getBigDecimalParameter("inpaumqty");
    String strOperativeUOM = info.getStringParameter("inpcAum");
    String strBaseUOM = info.getStringParameter("inpcUomId");
    String mProductId = info.getStringParameter("inpmProductId");
    OBContext.setAdminMode(false);

    try {
      if (UOMUtil.isUomManagementEnabled()) {
        if (strOperativeUOM == null || strOperativeUOM.isEmpty()) {
          qty = null;
        } else if (!strOperativeUOM.equals(strBaseUOM)) {
          qty = UOMUtil.getConvertedQty(mProductId, qty, strOperativeUOM);
        }
      }
    } catch (OBException e) {
      logger.error("Unable to convert Operative Qty to base qty");
      qty = null;
    } finally {
      OBContext.restorePreviousMode();
      if (StringUtils.equals(strWindowId, DOi_ID)) {
        info.addResult("inpqtyconfirmed", qty);
      } else if (StringUtils.equals(strWindowId, DOr_ID)) {
        info.addResult("inpqtyordered", qty);
      }
    }
  }

}
